// mock
const BASE_URL = 'https://mock.mengxuegu.com/mock/6310d2aeec1c7c261a3b7cbb/kp'

export const myRequest = (options) => {
	return new Promise((resolve, reject) => {
		uni.showLoading({
			title: '数据加载中',
			mask: true
		})
		uni.request({
			url: BASE_URL + options.url,
			method: options.method || 'GET',
			data: options.data || {},
			success:(res) => {
				if(res.statusCode !== 200){
					// console.log(res)
					return uni.showToast({
						title: '获取数据失败'
					})
				}
				resolve(res)
			},
			fail:(err) => {
				uni.showToast({
					title: '请求接口失败'
				})
				reject(err)
			}
		})
		setTimeout(() => {
			uni.hideLoading()
		}, 400)
	})
}